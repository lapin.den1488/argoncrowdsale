pragma solidity ^0.4.24;

import "../Openzeppelin/Tokens/ERC20/ERC20Mintable.sol";
import "../Openzeppelin/Tokens/ERC20/ERC20Burnable.sol";

contract Argon is ERC20Mintable, ERC20Burnable {
    string public name = "Argon coin";
    string public symbol = "ARG";
    uint8 public decimals = 18;
}