using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using HackatonApiCoreNethereum.Helpers;
using HackatonApiCoreNethereum.Models.Enums;
using HackatonApiCoreNethereum.Models.Identity;
using HackatonApiCoreNethereum.Models.Identity.Context;
using HackatonApiCoreNethereum.Models.Identity.Files;
using HackatonApiCoreNethereum.Models.Identity.User;
using HackatonApiCoreNethereum.Models.ViewModels.User;
using HackatonApiCoreNethereum.Services.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Nethereum.Web3;

namespace HackatonApiCoreNethereum.Services
{
    public class UserService : IUserService
    {
        private readonly Web3 web;

        private readonly string argonTokenAbi; 
        private readonly string argonTokenAddress; 

        private readonly UserManager<ApplicationUser> userManager;
        private readonly ApplicationDbContext dbContext;
        private readonly IMapper mapper;
        IHostingEnvironment appEnvironment;

        public UserService(UserManager<ApplicationUser> userManager, ApplicationDbContext dbContext,
         IMapper mapper, IHostingEnvironment appEnvironment, IConfiguration config)
        {
            this.userManager = userManager;
            this.dbContext = dbContext;
            this.mapper = mapper;
            this.appEnvironment = appEnvironment;

            this.web = new Web3(config["BlockchainSetting:Web3Url"]);
            this.argonTokenAddress = config["BlockchainSetting:ArgonTokenAddress"];
            this.argonTokenAbi = config["BlockchainSetting:ArgonTokenAbi"];
        }

        #region Methods

        public Task<IdentityResult> ConfirmMail(string id, string code)
        {
            throw new System.NotImplementedException();
        }

        public async Task<IdentityResult> CreateAsync(CreateUserViewModel model)
        {
            try
            {
                var user = mapper.Map<CreateUserViewModel, ApplicationUser>(model);
                var creationResult = await userManager.CreateAsync(user, model.Password);
                if (!creationResult.Succeeded)
                    return creationResult;

                if (model.ProfileImage != null)
                {
                    var imageResult = await AddProfileImageAsync(user, model.ProfileImage);
                    if (!imageResult.Succeeded)
                        return imageResult;
                }

                var toRoleResult = await userManager.AddToRoleAsync(user, Role.User.ToString());
                if (!toRoleResult.Succeeded)
                    return creationResult;

                return toRoleResult;
            }
            catch (System.Exception exeption)
            {
                var error = new IdentityError();
                error.Description = exeption.Message;
                return IdentityResult.Failed(error);
            }
        }

        public async Task<IdentityResult> AddProfileImageAsync(ApplicationUser user, IFormFile image)
        {
            try
            {
                var currentImage = await dbContext.ProfileImages.FirstOrDefaultAsync(x=>x.Id==user.Id);
                if(currentImage != null){
                    File.Delete(appEnvironment.WebRootPath+currentImage.Path);
                    dbContext.ProfileImages.Remove(currentImage);
                    await dbContext.SaveChangesAsync();
                }
                
                string transformedName = user.Id + GetFormat(image);
                string path = "/ProfileImages/"+transformedName;
                using (var fileStream = new FileStream(appEnvironment.WebRootPath + path,
                 FileMode.Create))
                {
                    await image.CopyToAsync(fileStream);
                }
                ProfileImage file = new ProfileImage {Name = transformedName ,Path = path,
                 User = user };
                await dbContext.ProfileImages.AddAsync(file);
                await dbContext.SaveChangesAsync();

                return IdentityResult.Success;
            }
            catch (System.Exception exeption)
            {
                var error = new IdentityError();
                error.Description = exeption.Message;
                return IdentityResult.Failed(error);
            }
        }

        private string GetFormat(IFormFile file){
            try
            {
               var fileName = file.FileName;
               var position = fileName.IndexOf('.');
               var result = fileName.Substring(position);

               return result;
            }
            catch (System.Exception)
            {
                return string.Empty;
            }
        }
        public async Task<IdentityResult> DeleteAsync(string name)
        {
            try
            {
                var user = await userManager.FindByNameAsync(name);
                //if(user==null)
                //    return IdentityResult.Failed(new IdentityError().Description());
                var deleteResult = await userManager.DeleteAsync(user);

                return deleteResult;
            }
            catch (System.Exception exeption)
            {
                var error = new IdentityError();
                error.Description = exeption.Message;
                return IdentityResult.Failed(error);
            }
        }

        public void Dispose()
        {
            userManager.Dispose();
        }

        public async Task<IdentityResult> EditAsync(string name, EditUserViewModel model)
        {
            try
            {
                var user = await userManager.FindByNameAsync(name);
                user.Email = model.Email;
                user.UserName = model.UserName;
                user.PublicAddress = model.PublicAddress;

                if (model.ProfileImage != null)
                {
                    var imageResult = await AddProfileImageAsync(user, model.ProfileImage);
                    if (!imageResult.Succeeded)
                        return imageResult;
                }

                var result = await userManager.UpdateAsync(user);
                //await dbContext.SaveChangesAsync();

                return result;
            }
            catch (System.Exception exeption)
            {
                var error = new IdentityError();
                error.Description = exeption.Message;
                return IdentityResult.Failed(error);
            }
        }

        public async Task<ClaimsIdentity> GetClaimsIdentity(LoginUserViewModel model)
        {
            try
            {
                var user = await userManager.FindByNameAsync(model.UserName);
                var result = await userManager.CheckPasswordAsync(user, model.Password);
                if (result == false)
                    return null;

                var userRole = await userManager.GetRolesAsync(user);
                var claims = new List<Claim>{
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.UserName),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, userRole[0]),
                new Claim("Wallet address", user.PublicAddress)
            };
                var claimsIdentity = new ClaimsIdentity(claims, "Token",
                ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType
                );

                return claimsIdentity;
            }
            catch (System.Exception)
            {
                return new ClaimsIdentity();
            }
        }

        public async Task<CustomIdentityResult> GetInfoAsync(string name)
        {
            try
            {
                var user = await userManager.FindByNameAsync(name);
                var mappedUser = mapper.Map<ApplicationUser, DetailsUserViewModel>(user);

                var argonContract = web.Eth.GetContract(argonTokenAbi, argonTokenAddress);
                var balanceOfFunc = argonContract.GetFunction("balanceOf");
                var balance = await balanceOfFunc.CallAsync<int>(user.PublicAddress, null, null, user.PublicAddress);

                mappedUser.ArgonBalance = balance;

                var image = await dbContext.ProfileImages.FirstOrDefaultAsync(x=>x.Id == user.Id);
                if(image == null)
                    mappedUser.ProfileImagePath = null;
                else                    
                    mappedUser.ProfileImagePath = image.Path;

                return new CustomIdentityResult(true, mappedUser);
            }
            catch (System.Exception e)
            {
                return new CustomIdentityResult(false, e.Message);
            }
        }

        public async Task<CustomIdentityResult> GetList()
        {
            try
            {
                var usersList = dbContext.Users.ToList();
                var mappedList = mapper.Map<List<ApplicationUser>, List<ListUserViewModel>>(usersList);

                foreach(var item in mappedList){
                    var image = await dbContext.ProfileImages.FirstOrDefaultAsync(x=>x.Id == item.Id);
                    if(image == null)
                        item.ProfileImagePath = null;
                    else                    
                        item.ProfileImagePath = image.Path;                    
                }

                return new CustomIdentityResult(true,  mappedList);
            }
            catch (System.Exception e)
            {
                return new CustomIdentityResult(false, e.Message);
            }
        }

        public async Task<string> GetToken(LoginUserViewModel model)
        {
            try
            {
                var claimsIdentity = await GetClaimsIdentity(model);
                if (claimsIdentity == null)
                    return null;

                var securityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes("_-SomethingForEncoding-_"));
                var token = new JwtSecurityToken(
                        issuer: "J3W",
                        audience: "http://localhost:5000",
                        notBefore: DateTime.Now,
                        claims: claimsIdentity.Claims,
                        expires: DateTime.Now.Add(TimeSpan.FromMinutes(60)),
                        signingCredentials: new SigningCredentials(securityKey,
                         SecurityAlgorithms.HmacSha256));

                var encodedJwt = new JwtSecurityTokenHandler().WriteToken(token);

                return encodedJwt;
            }
            catch (System.Exception)
            {
                return string.Empty;
            }
        }

        public Task SendConfirmMessageAsync(string id)
        {
            throw new System.NotImplementedException();
        }

        public async Task<string> GetPublicAddress(string username)
        {
            //var user = await userManager.FindByNameAsync(username);
            try
            {
                var user = await dbContext.Users.FirstOrDefaultAsync(x => x.UserName == username);
                return user.PublicAddress;
            }
            catch (System.Exception)
            {
                return string.Empty;
            }
        }

        public async Task<string> GetUsernameByAddress(string userAddress)
        {
            try
            {
                var user = await dbContext.Users.FirstOrDefaultAsync(x => x.PublicAddress == userAddress);
                return user.UserName;
            }
            catch (System.Exception)
            {
                return string.Empty;
            }
        }

        #endregion
    }
}