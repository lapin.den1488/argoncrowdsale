using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using HackatonApiCoreNethereum.Helpers;
using HackatonApiCoreNethereum.Models.Identity;
using HackatonApiCoreNethereum.Models.Identity.User;
using HackatonApiCoreNethereum.Models.ViewModels.User;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace HackatonApiCoreNethereum.Services.Interfaces{
    public interface IUserService:IDisposable
    {
        #region Create 
        Task<IdentityResult> CreateAsync(CreateUserViewModel model);
        #endregion

        #region Edit 
        Task<IdentityResult> EditAsync(string name,EditUserViewModel model);
        #endregion

        #region Delete
        Task<IdentityResult> DeleteAsync(string name);
        #endregion

        #region GetList
        Task<CustomIdentityResult> GetList();
        #endregion

        #region GetUser
        Task<CustomIdentityResult> GetInfoAsync(string name);
        
        #endregion

        #region EmailConfirmation
        Task SendConfirmMessageAsync(string id);
        Task<IdentityResult> ConfirmMail(string id, string code);
        #endregion

        #region GetClaimIdentity

        Task<ClaimsIdentity> GetClaimsIdentity(LoginUserViewModel model);

        #endregion

        #region GetToken

        Task<string> GetToken(LoginUserViewModel model);
            
        #endregion
        #region GetPublicAddress

        Task<string> GetPublicAddress(string username);
            
        #endregion
        #region GetUsernameByAddress

        Task<string> GetUsernameByAddress(string userAddress);

        #endregion
        #region Add images
            Task<IdentityResult> AddProfileImageAsync(ApplicationUser user, IFormFile image);
        #endregion
    }
}