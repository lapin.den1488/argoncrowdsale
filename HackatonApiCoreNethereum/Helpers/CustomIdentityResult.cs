using System.Collections;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace HackatonApiCoreNethereum.Helpers{
    public class CustomIdentityResult : IdentityResult{
        public CustomIdentityResult(bool success,string message):base(){
            this.Message = message;
            this.Succeeded = success;           
        }

        public CustomIdentityResult(bool success,object obj):base(){            
            this.Succeeded = success;
            this.Object = obj;      
        }
        
        public object Object{get;set;}
        public string Message { get; set; }
        
    }
}