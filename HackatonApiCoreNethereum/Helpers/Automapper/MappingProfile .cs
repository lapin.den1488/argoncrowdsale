using AutoMapper;
using HackatonApiCoreNethereum.Models.Identity.User;
using HackatonApiCoreNethereum.Models.ViewModels.User;

namespace HackatonApiCoreNethereum.Automapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<ApplicationUser, ListUserViewModel>();
            CreateMap<ApplicationUser, DetailsUserViewModel>();
            CreateMap<CreateUserViewModel, ApplicationUser>().ForMember(x => x.ProfileImage,
             opt => opt.Ignore());    
        }
    }
}