using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace HackatonApiCoreNethereum.Models.ViewModels.User{
    public class EditUserViewModel{
        
        [Required]
        [Display(Name = "Username")]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
        
        [Required]        
        [Display(Name = "Public Address")]
        public string PublicAddress{get;set;} 
      
        [Display(Name = "Phone Number")]
        public string PhoneNumber{get;set;}

        [DataType(DataType.Upload)]
        [Display(Name = "Profile Image")]
        //[FileExtensions()]
        public IFormFile ProfileImage {get;set;}

        [Display(Name = "Organization")]        
        public string Organization { get; set; }
    }
}