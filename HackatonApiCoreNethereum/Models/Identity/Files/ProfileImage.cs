using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using HackatonApiCoreNethereum.Models.Identity.User;

namespace HackatonApiCoreNethereum.Models.Identity.Files
{
    public class ProfileImage
    {
        [Key]
        [ForeignKey("User")]
        public string Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public ApplicationUser User { get; set; }
    }
}